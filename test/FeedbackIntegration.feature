Feature: Feedback Integration

Scenario: Adding an entry to Feedback resolves
  Given is a feedback service that takes in data
  When the feedback data is entered in
  Then the function resolves

Scenario: Write input name data to file
  Given is a feedback service that takes in data
  When the feedback data is entered in
  And a name parameter is provided
  Then the name data is written to the test json file
