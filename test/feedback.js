const assert = require( 'assert' );
const express = require('express');
const configs = require('../server/config');
const FeedbackService = require('../server/services/FeedbackService');
const app = express();
const fs = require('fs');
//const path = require('path');


//Central CONFIG
const config = configs[app.get('env')];
//instantiate feedback class
const feedbackService = new FeedbackService(config.data.testFeedback);
var testName = Math.random();
var testTitle = "This is a title!";
var testMessage ="A message in a bottle"
const testFilePath = './test/testFeedback.json';

describe( 'Feedback Service', () => {

    it('should resolve', () => {
      return feedbackService.addEntry(testName, testTitle, testMessage);;
    });

    it('should write input name data to file', async () => {

       await feedbackService.addEntry(testName, testTitle, testMessage);
       var buf = fs.readFileSync(testFilePath, "utf8");
       var jsonObject= JSON.parse(buf);

       assert.strictEqual( jsonObject[0].name, testName );

     })

      afterEach(function() {
      //clean up
      fs.writeFile(testFilePath, '', function(){console.log('done')});
      });
})
