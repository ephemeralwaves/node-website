# NodeJS PGH Meetup Site

[![LICENSE](https://img.shields.io/badge/license-MIT-lightgrey.svg)](https://github.com/h5bp/html5-boilerplate/blob/master/LICENSE.txt)

Update: the site rebranded and this is now defunct
----

To Run:

`npm i`

`npm start`

----
Docker Instructions:

`docker pull ephemeralwaves/nodepghserver:1.2.1`

`docker run -i -p 3000:3000 -t nodepghserver:1.2.1`

or

Build locally (As root) with:

`docker build -t nodepghserver-run -f Dockerfile-run .`

and run :
`docker run -i -p 3000:3000 -t nodepghserver-run`

----
